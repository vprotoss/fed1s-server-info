remote.add_interface("fed1s-server-info", {
  informatron_menu = function(data)
    return menu(data.player_index)
  end,
  informatron_page_content = function(data)
    return page_content(data.page_name, data.player_index, data.element)
  end
})


function menu(player_index)
  return {
    server_rules=1,
    blueprints=1,
    railways={
      stations_rules = 1,
      resources_stations_rules = 1,
      cityblocks = {
        cityblocks_naming=1,
        cityblocks_on_ground=1
      },
    }
  }
end


function page_content(page_name, player_index, element)
  -- main page
  if page_name == "server_rules" then
    element.add{type="label", name="text_1", caption={"fed1s-server-info.page_server_rules_text_1"}}
  end

  if page_name == "blueprints" then
    element.add{type="label", name="text_1", caption={"fed1s-server-info.page_blueprints_text_1"}}
    element.add{type="button", name="image_1", style="blueprints"}
  end

  if page_name == "railways" then
    element.add{type="label", name="text_1", caption={"fed1s-server-info.page_railways_text_1"}}
  end

  if page_name == "stations_rules" then
    element.add{type="label", name="text_1", caption={"fed1s-server-info.page_stations_rules_text_1"}}
    element.add{type="button", name="image_1", style="stations_rules"}
  end

  if page_name == "resources_stations_rules" then
    element.add{type="label", name="text_1", caption={"fed1s-server-info.page_resources_stations_rules_text_1"}}
    element.add{type="button", name="image_1", style="resources_stations_rules"}
  end

  if page_name == "cityblocks" then
    element.add{type="label", name="text_1", caption={"fed1s-server-info.page_cityblocks_text_1"}}
    element.add{type="button", name="image_1", style="cityblocks"}
  end

  if page_name == "cityblocks_naming" then
    element.add{type="label", name="text_1", caption={"fed1s-server-info.page_cityblocks_naming_text_1"}}
    element.add{type="button", name="image_1", style="cityblocks_naming"}
  end

  if page_name == "cityblocks_on_ground" then
    element.add{type="label", name="text_1", caption={"fed1s-server-info.page_cityblocks_on_ground_text_1"}}
    element.add{type="button", name="image_1", style="cityblocks_on_ground"}
  end

end
